* Create new project:
```
cargo new --lib wasm
```

* Modify cargo.toml:
```
[lib]
crate-type = ["cdylib"]
```

* Add wasm32 target from rustup (if first time):
```
rustup target add wasm32-unknown-unknown
```

* Install wasm-gc (.wasm cleaner to make smaller binaries)
```
cargo install wasm-gc
```

* Install http server, that serves working folder:
```
cargo install https
```

* Create the wasm with one of the two:
```
cargo build --target wasm32-unknown-unknown
cargo build --release --target=wasm32-unknown-unknown
```

* Clean the used wasm
```
wasm-gc target/wasm32-unknown-unknown/release/wasm.wasm -o wasm.gc.wasm
```

* Create index.html (https://gitlab.com/lightern/rust/blob/master/wasm/index.html) and link that to cleaned wasm

* Serve the file with this command from command prompt:
```
http
```

* Visit localhost:8000
 

More info: https://dev.to/h_ajsf/simple-rust--wasm-example-5b1m