extern crate wasm_bindgen;
extern crate web_sys;

use std::ops::Add;
use wasm_bindgen::prelude::*;
use wasm_bindgen::Clamped;
use web_sys::{CanvasRenderingContext2d, ImageData};


#[wasm_bindgen]
pub extern fn add_one(x: u32) -> u32 {
    x + 1
}

#[wasm_bindgen]
pub extern fn draw(
    ctx: &CanvasRenderingContext2d,
    width: u32,
    height: u32,
) -> Result<(), JsValue> {
    // The real workhorse of this algorithm, generating pixel data
    let mut data = Vec::new();

    for x in 0..width {
        for y in 0..height {
            data.push(10 as u8);
            data.push(0 as u8);
            data.push(0 as u8);
            data.push(255);
        }
    }
    let data1 = web_sys::ImageData::new_with_u8_clamped_array_and_sh(Clamped(&mut data), width, height)?;
    ctx.put_image_data(&data1, 0.0, 0.0)
}