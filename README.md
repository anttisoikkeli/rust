# Rust

I've written some stuff to learn Rust. Hope these are useful or helpful in your own studies.

# Install in Arch
* https://rustup.rs/

# To make CLion debugging work in Windows:
* Install mingw-w64 (package of precompiled binaries for Windows): https://sourceforge.net/projects/mingw-w64/

# Diesel in Windows 10
Copy from:
```
C:\Program Files\PostgreSQL\11\lib
```
Files libpq.dll and libpq.lib to:
```
C:\Users\username\.rustup\toolchains\stable-x86_64-pc-windows-gnu\lib\rustlib\x86_64-pc-windows-gnu\lib
```