fn checkrow(i: usize, sudoku: &[usize]) -> bool {
    let row_start = i - i % 9;
    let row = &sudoku[row_start .. row_start+9];
    row.iter().filter(|x| x == &&sudoku[i]).count() == 1
}

fn checkcolumn(i: usize, sudoku: &[usize]) -> bool {
    let column: Vec<usize> = (0..9).into_iter().map(|x| sudoku[(i % 9) + (x * 9)]).collect();
    column.iter().filter(|x| x == &&sudoku[i]).count() == 1
}

fn checkbox(i: usize, sudoku: &[usize]) -> bool {
    let box_column = (i % 9) - (i % 9) % 3;
    let box_row = (i - (i % 27))/ 27;
    let box_values: Vec<usize> = [0,1,2,9,10,11,18,19,20].into_iter().map(|x| sudoku[27*box_row + box_column + x]).collect();
    box_values.iter().filter(|x| x == &&sudoku[i]).count() == 1
}

fn main() {
    let values = vec![8,0,3,0,4,0,0,7,5,4,0,0,3,0,0,0,6,8,0,0,0,9,5,0,0,0,3,5,0,0,0,0,2,7,0,0,6,0,2,8,0,4,0,0,9,0,0,9,0,7,0,4,3,0,7,0,8,0,0,1,0,0,0,3,0,0,0,0,0,0,0,7,0,4,6,0,2,0,0,0,0];
    let mut sudoku = values.to_vec();
    let mut i: usize = 0;
    let mut direction = true;

    loop {
        if values[i] == 0 {
            if sudoku[i] < 9 {
                sudoku[i] += 1;
                direction = true;
                if checkrow(i, &sudoku) && checkcolumn(i, &sudoku) && checkbox(i, &sudoku) {
                    if i == 80 {
                        for x in 0..9 {
                            println!("  {} {} {} | {} {} {} | {} {} {}", sudoku[0+x*9], sudoku[1+x*9], sudoku[2+x*9], sudoku[3+x*9], sudoku[4+x*9], sudoku[5+x*9], sudoku[6+x*9], sudoku[7+x*9], sudoku[8+x*9]);
                        }
                        break
                    } else {
                        i += 1;
                    }
                }
            } else {
                sudoku[i] = 0;
                i -= 1;
                direction = false;
            }
        } else if direction == true {
            if i == 80 {
                for x in 0..9 {
                    println!("  {} {} {} | {} {} {} | {} {} {}", sudoku[0+x*9], sudoku[1+x*9], sudoku[2+x*9], sudoku[3+x*9], sudoku[4+x*9], sudoku[5+x*9], sudoku[6+x*9], sudoku[7+x*9], sudoku[8+x*9]);
                }
                break
            } else {
                i += 1;
                direction = true;
            }
        } else {
            if i == 0 {
                println!{"No answer!"};
                break
            }
            else {
                i -= 1;
                direction = false
            }
        }
    }
}